# Merge - [![version](https://img.shields.io/npm/v/@typemon/merge.svg)](https://www.npmjs.com/package/@typemon/merge) [![license](https://img.shields.io/npm/l/@typemon/merge.svg)](https://gitlab.com/monster-space-network/typemon/merge/blob/master/LICENSE) ![typescript-version](https://img.shields.io/npm/dependency-version/@typemon/merge/dev/typescript.svg) [![gitlab-pipeline](https://gitlab.com/monster-space-network/typemon/merge/badges/master/pipeline.svg)](https://gitlab.com/monster-space-network/typemon/merge/-/pipelines) [![coverage](https://gitlab.com/monster-space-network/typemon/merge/badges/master/coverage.svg)](https://gitlab.com/monster-space-network/typemon/merge/-/graphs/master/charts)



## Installation
```
$ npm install @typemon/merge
```



## Usage
```typescript
import { merge } from '@typemon/merge';
```

### Basic
```typescript
merge(
    { foo: 'bar' },
    { bar: 'foo' },
);
```
```typescript
{
    foo: 'bar',
    bar: 'foo'
}
```

### Advanced
```typescript
merge(
    {
        name: 'Adventurer',
        level: 7,
        items: [
            'sword',
        ],
        pet: null,
    },
    {
        items: [
            'knife',
            'bread',
        ],
        pet: {
            name: 'Monster',
            level: 3,
            eat: (): void => console.log('Yum Yum ...'),
        },
    },
);
```
```typescript
{
    name: 'Adventurer',
    level: 7,
    items: [
        'knife',
        'bread'
    ],
    pet: {
        name: 'Monster',
        level: 3,
        eat: [Function: eat]
    }
}
```



## Logic
- If the previous and current values ​​are objects, but not an array, they are merged.
- If the above conditions are not met, the current value is used.
