//
//
//
function isMergeable(value: unknown): boolean {
    return typeof value === 'object' && value !== null && !Array.isArray(value);
}

export function merge(...resources: ReadonlyArray<object>): any {
    if (resources.length < 2) {
        throw new Error('Provide at least two resources.');
    }

    const result: any = {};

    for (const resource of resources) {
        for (const [key, nextableValue] of Object.entries(resource)) {
            const previousValue: any = result[key];
            const nextValue: unknown = isMergeable(previousValue) && isMergeable(nextableValue)
                ? merge(previousValue, nextableValue)
                : nextableValue;

            result[key] = nextValue;
        }
    }

    return result;
}
