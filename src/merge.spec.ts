import { merge } from '../src';
//
//
//
test('error', () => {
    expect(() => merge()).toThrow('Provide at least two resources.');
    expect(() => merge({})).toThrow('Provide at least two resources.');
});

test('basic', () => {
    expect(merge(
        {
            foo: 'bar'
        },
        {
            bar: 'foo'
        }
    )).toMatchObject({
        foo: 'bar',
        bar: 'foo'
    });
});

test('advanced', () => {
    expect(merge(
        {
            b1: false,
            n1: 0,
            s1: '',
            a1: [],
            o1: {
                b1: false,
                n1: 0,
                s1: '',
                a1: []
            },
            o2: {
                a1: [],
                a2: []
            }
        },
        {
            b2: true,
            n2: 1,
            s2: ' ',
            a2: [
                null
            ],
            o1: {
                b2: true,
                n2: 1,
                s2: ' ',
                a2: [
                    null
                ]
            },
            o2: {
                a1: null,
                a2: [
                    null
                ]
            }
        }
    )).toMatchObject({
        b1: false,
        n1: 0,
        s1: '',
        a1: [],
        b2: true,
        n2: 1,
        s2: ' ',
        a2: [
            null
        ],
        o1: {
            b1: false,
            n1: 0,
            s1: '',
            a1: [],
            b2: true,
            n2: 1,
            s2: ' ',
            a2: [
                null
            ]
        },
        o2: {
            a1: null,
            a2: [
                null
            ]
        }
    });
});
