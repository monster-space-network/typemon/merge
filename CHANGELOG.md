# [3.1.0](https://gitlab.com/monster-space-network/typemon/merge/compare/3.0.0...3.1.0) (2020-07-30)


### Bug Fixes

* 누락된 null 검사 추가 ([1701afa](https://gitlab.com/monster-space-network/typemon/merge/commit/1701afa29ce44170f349fa2019977be1f31a6401))


### Features

* @typemon/check 의존성 삭제 ([39ddf50](https://gitlab.com/monster-space-network/typemon/merge/commit/39ddf50e981af981894b6894f13b2a921fde728f))
* 의존성 변경 사항 반영 ([c6bf158](https://gitlab.com/monster-space-network/typemon/merge/commit/c6bf1587d2a9d75e663986d39ab5f4a4a3899f6c))



# [3.0.0](https://gitlab.com/monster-space-network/typemon/merge/compare/2.0.0...3.0.0) (2020-02-12)


### Features

* 의존성 업데이트 ([3e5e0f3](https://gitlab.com/monster-space-network/typemon/merge/commit/3e5e0f35118600156e3637391d61d339fbb0e71a))
* 재작성 ([fbde778](https://gitlab.com/monster-space-network/typemon/merge/commit/fbde778acd6e1d93b1131cc7b328acf8f4b24f99))



# [2.0.0](https://gitlab.com/monster-space-network/typemon/merge/compare/1.0.0...2.0.0) (2019-10-17)


### Features

* 기능 축소 및 개선 ([aa7cc6e](https://gitlab.com/monster-space-network/typemon/merge/commit/aa7cc6ead29792be25edbe7336156cc287afa48d))
* 의존성 추가 및 업데이트 ([af5de24](https://gitlab.com/monster-space-network/typemon/merge/commit/af5de24ca4b59e91c1b1503c398d49efccc880b6))
